# MathLibary

[![CI Status](https://img.shields.io/travis/kahvecioglume/MathLibary.svg?style=flat)](https://travis-ci.org/kahvecioglume/MathLibary)
[![Version](https://img.shields.io/cocoapods/v/MathLibary.svg?style=flat)](https://cocoapods.org/pods/MathLibary)
[![License](https://img.shields.io/cocoapods/l/MathLibary.svg?style=flat)](https://cocoapods.org/pods/MathLibary)
[![Platform](https://img.shields.io/cocoapods/p/MathLibary.svg?style=flat)](https://cocoapods.org/pods/MathLibary)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MathLibary is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MathLibary'
```

## Author

Mert kahvecioğlu, m.kahvecioglu@vispera.co

## License

MathLibary is available under the MIT license. See the LICENSE file for more info.
